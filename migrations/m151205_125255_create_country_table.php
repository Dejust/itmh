<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_125255_create_country_table extends Migration
{
    public function up()
    {

        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], 'ENGINE = InnoDB');

        $this->createTable('country_video', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'video_id' => $this->integer()->notNull()
        ], 'ENGINE = InnoDB');

        $this->addForeignKey('FK_country_video__county', 'country_video', 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_country_video__video', 'country_video', 'video_id', 'video', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('country');
        $this->dropTable('country_video');
    }
}
