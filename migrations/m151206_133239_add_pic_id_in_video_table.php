<?php

use yii\db\Schema;
use yii\db\Migration;

class m151206_133239_add_pic_id_in_video_table extends Migration
{
    public function up()
    {
        $this->addColumn('video', 'pic_id', $this->string());
    }

    public function down()
    {
        $this->dropColumn('video', 'pic_id');
    }
}
