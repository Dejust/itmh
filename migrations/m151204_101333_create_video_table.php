<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_101333_create_video_table extends Migration
{
    public function up()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'origin_title' => $this->string()->notNull()->defaultValue(''),
            'year_start_filming' => $this->integer()->notNull(),
            'year_end_filming' => $this->integer()->notNull(),
            'duration' => $this->integer()->notNull(),
            'premiere_date' => $this->date()->notNull(),
            'annonce' => $this->text()->notNull()->defaultValue(''),
            'description' => $this->text()->notNull()->defaultValue('')
        ], 'ENGINE = InnoDB');
    }

    public function down()
    {
        $this->dropTable('video');
    }
}
