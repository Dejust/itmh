<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_071717_create_user_table extends Migration
{
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
