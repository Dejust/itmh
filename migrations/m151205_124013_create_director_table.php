<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_124013_create_director_table extends Migration
{
    public function up()
    {
        $this->createTable('director', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], 'ENGINE = InnoDB');

        $this->createTable('director_video', [
            'id' => $this->primaryKey(),
            'director_id' => $this->integer()->notNull(),
            'video_id' => $this->integer()->notNull()
        ], 'ENGINE = InnoDB');

        $this->addForeignKey('FK_director_video__director', 'director_video', 'director_id', 'director', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_director_video__video', 'director_video', 'video_id', 'video', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('director');
        $this->dropTable('director_video');
    }
}
