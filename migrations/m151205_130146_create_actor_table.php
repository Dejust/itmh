<?php

use yii\db\Schema;
use yii\db\Migration;

class m151205_130146_create_actor_table extends Migration
{
    public function up()
    {
        $this->createTable('actor', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], 'ENGINE = InnoDB');

        $this->createTable('actor_video', [
            'id' => $this->primaryKey(),
            'actor_id' => $this->integer()->notNull(),
            'video_id' => $this->integer()->notNull()
        ], 'ENGINE = InnoDB');

        $this->addForeignKey('FK_actor_video__actor', 'actor_video', 'actor_id', 'actor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_actor_video__video', 'actor_video', 'video_id', 'video', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('actor');
        $this->dropTable('actor_video');
    }
}
