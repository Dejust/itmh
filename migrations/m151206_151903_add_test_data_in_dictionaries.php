<?php

use app\models\user\User;
use yii\db\Schema;
use yii\db\Migration;

class m151206_151903_add_test_data_in_dictionaries extends Migration
{
    public function up()
    {
        $this->createCountry('Россия');
        $this->createCountry('США');
        $this->createCountry('Англия');
        $this->createCountry('Япония');

        $this->createActor('Актер А');
        $this->createActor('Актер Б');
        $this->createActor('Актер В');
        $this->createActor('Актер Г');

        $this->createDirector('Директор А');
        $this->createDirector('Директор Б');
        $this->createDirector('Директор В');
        $this->createDirector('Директор Г');

        $user = new User();
        $user->setCredentials('test@example.com', '123456');
        $user->save();
    }

    public function down()
    {
    }

    private function createCountry($name)
    {
        (new \app\models\country\Country([
            'name' => $name
        ]))->save();
    }

    private function createActor($name)
    {
        (new \app\models\actor\Actor([
            'name' => $name
        ]))->save();
    }

    private function createDirector($name)
    {
        (new \app\models\director\Director([
            'name' => $name
        ]))->save();
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
