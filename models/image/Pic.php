<?php

namespace app\models\image;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class Pic
{
    const THUMBNAIL_LARGE = 'large';
    const THUMBNAIL_SMALL = 'small';

    /** @var  string путь к оригинальному изображению */
    private $originPath;

    /** @var  string путь к превью изображения */
    private $thumbnailsPath;

    private $identity;

    public function __construct($identity)
    {
        $this->identity = $identity;
    }

    public static function thumbnailsConfig()
    {
        return [
            self::THUMBNAIL_LARGE => [218, 150],
            self::THUMBNAIL_SMALL => [145, 100]
        ];
    }

    public static function name()
    {
        return 'video';
    }

    public function setOriginPath($path)
    {
        $this->originPath = $path;
    }

    public function getOriginPath()
    {
        return $this->originPath;
    }

    public function setThumbnailPath($kind, $path)
    {
        if (!isset(static::thumbnailsConfig()[$kind])) {
            throw new InvalidConfigException('Unknown kind of thumbnail ' . $kind);
        }

        $this->thumbnailsPath[$kind] = $path;
    }

    public function getThumbnailPath($kind)
    {
        if (!isset(static::thumbnailsConfig()[$kind])) {
            throw new InvalidConfigException('Unknown kind of thumbnail ' . $kind);
        }

        return ArrayHelper::getValue($this->thumbnailsPath, $kind, '');
    }

    public function getThumbnailPaths()
    {
        $kinds = array_keys(static::thumbnailsConfig());
        $paths = array_map([$this, 'getThumbnailPath'], $kinds);
        return array_combine($kinds, $paths);
    }

    public function getId()
    {
        return $this->identity;
    }

    public function equals(Pic $pic)
    {
        return $this->getId() === $pic->getId();
    }
}
