<?php

namespace app\models\image;

use Imagine\Image\ImageInterface;
use yii\base\Object;
use yii\imagine\Image;

class RawPic extends Object
{
    /**
     * Полное название класса изображений
     * Должен являться наследником app\models\image\Pic
     * @var string
     */
    public $picClass = 'app\models\image\Pic';

    /** @var  string путь к оригинальному изображению */
    private $path;

    public function __construct($path, array $config)
    {
        parent::__construct($config);

        $this->path = $path;
    }

    /**
     * @return ImageInterface
     */
    public function getOriginImage()
    {
        return Image::getImagine()
            ->open($this->path);
    }

    /**
     * @return ImageInterface[]
     */
    public function getThumbnails()
    {
        /** @var Pic $class */
        $class = $this->picClass;

        $thumbs = [];
        foreach ($class::thumbnailsConfig() as $kind => $sizes) {
            $thumbs[$kind] = Image::thumbnail($this->path, $sizes[0], $sizes[1]);
        }

        return $thumbs;
    }

    public function getImageExtension()
    {
        return pathinfo($this->path, PATHINFO_EXTENSION);
    }
}
