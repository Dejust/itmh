<?php

namespace app\models\image\web;

use yii\base\Model;
use yii\web\UploadedFile;

class ImageUploadForm extends Model
{
    /**
     * @var mixed
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return UploadedFile|bool|null
     */
    public function getUploadedImage()
    {
        if ($this->validate()) {
            return $this->imageFile;
        } else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'imageFile' => 'Изображение'
        ];
    }
}
