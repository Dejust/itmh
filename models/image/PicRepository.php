<?php

namespace app\models\image;

use Symfony\Component\Filesystem\Filesystem;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;

class PicRepository extends Component
{
    /** @var string полный путь к директории с изображениями */
    public $imagesDirPath = '@app/web/media/images';

    /** @var string путь к директории с изображениями, доступный из WEB */
    public $webAccessPath = 'media/images';

    /**
     * Полное название класса изображений
     * Должен являться наследником app\models\image\Pic
     * @var string
     */
    public $picClass = 'app\models\image\Pic';

    /** @var  Filesystem */
    private $filesystem;

    const ORIGIN_FILENAME = 'origin';

    const THUMBNAIL_PREFIX = 'thumb_';

    public function init()
    {
        parent::init();

        $this->filesystem = $this->filesystem ?: new Filesystem();

        $this->filesystem->mkdir($this->getImagesDirPath());
    }

    /**
     * Сохраняет изображение и его превью
     *
     * @param RawPic $pic
     * @return Pic
     */
    public function put(RawPic $pic)
    {
        $id = uniqid();
        $picContainerPath = $this->getPicContainer($id);
        $this->filesystem->mkdir($picContainerPath);

        $originImageName = $picContainerPath . '/' . self::ORIGIN_FILENAME . '.' . $pic->getImageExtension();
        $pic->getOriginImage()->save($originImageName);

        foreach ($pic->getThumbnails() as $kind => $thumb) {
            $thumb->save($picContainerPath . '/' . self::THUMBNAIL_PREFIX . $kind . '.' . $pic->getImageExtension());
        }

        return $this->createPic($id);
    }

    /**
     * Возвращает изображение по его идентификатору
     * Если изображение не найдено, возвращает NULL
     *
     * @param string $id
     * @return Pic|null
     */
    public function find($id)
    {
        $imageContainer = $this->getPicContainer($id);

        if ($this->filesystem->exists($imageContainer)) {
            return $this->createPic($id);
        } else {
            return null;
        }
    }

    /**
     * Удаляет существующее изображение
     *
     * @param Pic $pic
     */
    public function remove(Pic $pic)
    {
        $containerDir = $this->getPicContainer($pic->getId());
        if ($this->filesystem->exists($containerDir)) {
            $this->filesystem->remove($containerDir);
        }
    }

    /**
     * Создает экземпляр изображения из исходных файлов
     *
     * @param string $id идентификатор существующего изображения
     * @return Pic
     * @throws \yii\base\InvalidConfigException
     */
    private function createPic($id)
    {
        /** @var Pic $pic */
        $pic = \Yii::createObject($this->picClass, [$id]);
        $webAccessPath = $this->getPicWebAccessPath($id);

        foreach ($this->getPicImages($id) as $imageName) {
            $fullWebAccessName = $webAccessPath . '/' . $imageName;

            if ($this->isOriginImage($imageName)) {
                $pic->setOriginPath($fullWebAccessName);
            } else if ($this->isThumbnailImage($imageName)) {
                $kind = $this->extractThumbKind($imageName);
                $pic->setThumbnailPath($kind, $fullWebAccessName);
            }
        }

        return $pic;
    }

    private static function isThumbnailImage($imageName)
    {
        return StringHelper::startsWith($imageName, self::THUMBNAIL_PREFIX);
    }

    private static function isOriginImage($imageName)
    {
        return StringHelper::startsWith($imageName, self::ORIGIN_FILENAME);
    }

    private static function extractThumbKind($imageName)
    {
        $regex = '/^'.self::THUMBNAIL_PREFIX.'(?P<kind>.*)\..*$/';
        preg_match($regex, $imageName, $matches);
        return $matches['kind'];
    }

    private function getPicImages($id)
    {
        $imageNames = FileHelper::findFiles($this->getPicContainer($id), [
            'only' => [self::ORIGIN_FILENAME . '*', self::THUMBNAIL_PREFIX . '*']
        ]);
        return array_map('basename', $imageNames);
    }

    private function getPicWebAccessPath($id)
    {
        return $this->webAccessPath . '/' . $this->getFullImageId($id);
    }

    private function getPicContainer($id)
    {
        return $this->getImagesDirPath() . '/' . $this->getFullImageId($id);
    }

    private function getFullImageId($id)
    {
        /** @var  Pic $imageClass */
        $imageClass = $this->picClass;
        return $imageClass::name() . '/' . $id;
    }

    private function getImagesDirPath()
    {
        return \Yii::getAlias($this->imagesDirPath);
    }
}
