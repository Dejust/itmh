<?php

namespace app\models\image;

use Symfony\Component\Filesystem\Filesystem;
use yii\web\UploadedFile;

class PicUploader
{
    /** @var string полный путь к директории для загрузки изображений */
    public $tempDir = '@app/runtime/uploadedImages';

    /**
     * Полное название класса изображений
     * Должен являться наследником app\models\image\Pic
     * @var string
     */
    public $picClass = 'app\models\image\Pic';

    /** @var  Filesystem */
    private $fs;

    public function __construct(Filesystem $fs = null)
    {
        $this->fs = $fs ?: new Filesystem();
    }

    public function upload(UploadedFile $file)
    {
        $filename = $this->fs->tempnam($this->getUploadedImagesDir(), 'tmp');
        $this->fs->remove($filename);

        $imageFilename = $filename . '.' . $file->getExtension();
        $file->saveAs($imageFilename);

        /** @var PicRepository $repository */
        $repository = \Yii::createObject(PicRepository::className(), [[
            'picClass' => $this->picClass
        ]]);

        /** @var RawPic $rawPic */
        $rawPic = \Yii::createObject(RawPic::className(), [
            $imageFilename,
            ['picClass' => $this->picClass]
        ]);

        $pic = $repository->put($rawPic);

        $this->fs->remove($imageFilename);

        return $pic;
    }

    private function getUploadedImagesDir()
    {
        return \Yii::getAlias($this->tempDir);
    }
}
