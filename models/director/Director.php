<?php

namespace app\models\director;

use Yii;

/**
 * This is the model class for table "director".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DirectorVideo[] $directorVideos
 */
class Director extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'director';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorVideos()
    {
        return $this->hasMany(DirectorVideo::className(), ['director_id' => 'id']);
    }
}
