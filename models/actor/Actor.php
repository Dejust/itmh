<?php

namespace app\models\actor;

use Yii;

/**
 * This is the model class for table "actor".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ActorVideo[] $actorVideos
 */
class Actor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActorVideos()
    {
        return $this->hasMany(ActorVideo::className(), ['actor_id' => 'id']);
    }
}
