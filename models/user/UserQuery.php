<?php
/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 04.12.2015
 * Time: 12:34
 */

namespace app\models\user;


use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    /**
     * @param string $email
     * @return UserQuery
     */
    public function whereEmailIs($email)
    {
        return $this->andWhere(['email' => $email]);
    }
}