<?php
namespace app\models\user\web;

use app\models\user\User;
use Yii;
use yii\base\Model;

class SignUpForm extends Model
{
    public $email;

    public $password;

    public $confirmedPassword;

    public $rememberMe = true;

    public $duration = 3600;

    public function rules()
    {
        return [
            [['email', 'password', 'confirmedPassword'], 'required'],
            [['email'], 'email'],
            [['email'], 'emailMustBeUnique'],
            [['confirmedPassword'],
                'compare',
                'compareAttribute' => 'password',
                'operator' => '==='
            ],
            [['rememberMe'], 'boolean']
        ];
    }

    /**
     * @param string $attribute
     */
    public function emailMustBeUnique($attribute)
    {
        if ($this->hasErrors()) {
            return;
        }

        $emailExists = User::find()
            ->whereEmailIs($this->email)
            ->exists();

        if ($emailExists) {
            $this->addError($attribute, 'User with email ' . $this->email . ' is already exists');
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function signUp()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setCredentials($this->email, $this->password);

        if ($user->save()) {
            return Yii::$app->user->login($user, $this->rememberMe ? $this->duration : 0);
        }

        return false;
    }
}
