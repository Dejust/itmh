<?php

namespace app\models\user\web;

use app\models\user\User;
use Yii;
use yii\base\Model;

class SignInForm extends Model
{
    public $email;

    public $password;

    public $rememberMe = true;

    public $duration = 3600;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['email'], 'email'],
            [['rememberMe'], 'boolean'],
            [['password'], 'validatePassword']
        ];
    }

    /**
     * @param string $attribute
     */
    public function validatePassword($attribute)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * @return bool
     */
    public function signIn()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? $this->duration : 0);
        }
        return false;
    }

    /**
     * @return User
     */
    private function getUser()
    {
        if (!empty($this->email)) {
            return User::find()
                ->whereEmailIs($this->email)
                ->one();
        }

        return null;
    }
}
