<?php

namespace app\models\video;

use app\models\actor\Actor;
use app\models\country\Country;
use app\models\director\Director;
use app\models\image\Pic;
use app\models\image\PicRepository;
use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer $id
 * @property string $title
 * @property string $origin_title
 * @property integer $year_start_filming
 * @property integer $year_end_filming
 * @property integer $duration
 * @property string $premiere_date
 * @property string $annonce
 * @property string $description
 * @property string pic_id
 *
 * @property Country $country
 * @property Director director
 * @property Actor[] actors
 * @property Pic $pic
 */
class Video extends ActiveRecord
{
    /**
     * @param \DateTime $dateTime
     */
    public function setPremiereDate(\DateTime $dateTime)
    {
        $this->premiere_date = $dateTime->format('Y-m-d');
    }

    /**
     * @return \DateTime|null
     */
    public function getPremiereDate()
    {
        return \DateTime::createFromFormat('Y-m-d', $this->premiere_date);
    }

    /**
     * @param int $start number of years less (or equal) than end year
     * @param int $end
     */
    public function setFilmingPeriod($start, $end)
    {
        if ($start > $end) {
            throw new InvalidParamException('Start year must be less or equal than end year');
        }

        $this->year_start_filming = $start;
        $this->year_end_filming = $end;
    }

    /**
     * @return array
     */
    public function getFilmingPeriod()
    {
        return [$this->year_start_filming, $this->year_end_filming];
    }

    /**
     * @param int $duration number of seconds greater than 1
     */
    public function setDuration($duration)
    {
        if ($duration < 1) {
            throw new InvalidParamException('Duration must be greater than 1 second');
        }

        $this->duration = $duration;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return ActiveRecord
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])
            ->viaTable('country_video', ['video_id' => 'id']);
    }

    /**
     * @param Country $country
     */
    public function setCountry(Country $country)
    {
        if (!empty($this->country)) {
            $this->unlink('country', $this->country, true);
        }

        $this->link('country', $country);
    }

    /**
     * @return ActiveRecord
     */
    public function getDirector()
    {
        return $this->hasOne(Director::className(), ['id' => 'director_id'])
            ->viaTable('director_video', ['video_id' => 'id']);
    }

    /**
     * @param Director $director
     */
    public function setDirector(Director $director)
    {
        if (!empty($this->director)) {
            $this->unlink('director', $this->director, true);
        }

        $this->link('director', $director);
    }

    /**
     * @return ActiveRecord
     */
    public function getActors()
    {
        return $this->hasMany(Actor::className(), ['id' => 'actor_id'])
            ->viaTable('actor_video', ['video_id' => 'id']);
    }

    /**
     * @param Actor[] $actors
     */
    public function setActors(array $actors)
    {
        $currentActors = ArrayHelper::index($this->actors, 'id');
        $actors = ArrayHelper::index($actors, 'id');

        $deprecatedActors = array_diff(array_keys($currentActors), array_keys($actors));
        foreach ($deprecatedActors as $deprecatedActorId) {
            $this->unlink('actors', $currentActors[$deprecatedActorId], true);
        }

        $newActors = array_diff(array_keys($actors), array_keys($currentActors));
        foreach ($newActors as $newActorId) {
            $this->link('actors', $actors[$newActorId]);
        }
    }

    /**
     * @return Pic|null
     */
    public function getPic()
    {
        if (!empty($this->pic_id)) {
            return (new PicRepository())->find($this->pic_id);
        } else {
            return null;
        }
    }

    /**
     * @param Pic $pic
     */
    public function setPic(Pic $pic)
    {
        /** @var Pic $currentPic */
        $currentPic = $this->getPic();

        if ($currentPic && !$currentPic->equals($pic)) {
            (new PicRepository())->remove($currentPic);
        }

        $this->pic_id = $pic->getId();
    }

    public function removePic()
    {
        $currentPic = $this->getPic();
        if ($currentPic) {
            (new PicRepository())->remove($currentPic);
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->removePic();
            return true;
        } else {
            return false;
        }
    }


}
