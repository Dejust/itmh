<?php

namespace app\models\video;

use app\exceptions\PersistenceException;
use app\models\actor\Actor;
use app\models\country\Country;
use app\models\director\Director;
use app\models\image\Pic;

class ComplexVideoSaver
{
    /** @var Video */
    private $video;

    /** @var  Director */
    public $director;

    /** @var  Country */
    public $country;

    /** @var  Actor[] */
    public $actors;

    /** @var  Pic */
    public $pic;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function save()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($this->persistVideo()) {
                $transaction->commit();
            } else {
                $transaction->rollBack();
                throw new PersistenceException($this->video);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    private function persistVideo()
    {
        if ($this->pic) {
            $this->video->setPic($this->pic);
        }

        if (!$this->video->save()) {
            return false;
        }

        if ($this->actors) {
            $this->video->setActors($this->actors);
        }

        if ($this->country) {
            $this->video->setCountry($this->country);
        }

        if ($this->director) {
            $this->video->setDirector($this->director);
        }

        return true;
    }
}
