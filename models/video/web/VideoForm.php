<?php

namespace app\models\video\web;

use app\models\actor\Actor;
use app\models\country\Country;
use app\models\director\Director;
use app\models\video\Video;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class VideoForm extends Model
{
    const PREMIERE_DATE_FORMAT = 'd.m.Y';

    public $title;

    public $originTitle;

    public $yearStartFilming;

    public $yearEndFilming;

    public $duration;

    public $premiereDate;

    public $annonce;

    public $description;

    public $directorId;

    public $countryId;

    public $actorIds = [];

    const MIN_FILMING_PERIOD_YEAR = 1870;
    const MAX_FILMING_PERIOD_YEAR = 2199;

    /** @var Video  */
    private $video;
    
    public function __construct(Video $record, array $config = [])
    {
        parent::__construct($config);
        $this->initFormData($record);
    }

    public function rules()
    {
        return [
            [['title', 'yearStartFilming', 'yearEndFilming',
                    'duration', 'premiereDate', 'annonce', 'description',
                    'directorId', 'countryId', 'actorIds'],
                'required'
            ],
            [['title', 'originTitle', 'annonce', 'description'], 'string'],
            [['yearStartFilming', 'yearEndFilming'], 'integer',
                'min' => self::MIN_FILMING_PERIOD_YEAR,
                'max' => self::MAX_FILMING_PERIOD_YEAR
            ],
            [['yearStartFilming'], 'compare',
                'compareAttribute' => 'yearEndFilming',
                'operator' => '<=',
                'type' => 'number'
            ],
            [['duration'], 'integer', 'min' => 1],
            [['premiereDate'], 'date', 'format' => 'php:' . self::PREMIERE_DATE_FORMAT],
            [['directorId', 'countryId'], 'integer'],
            [['actorIds'], 'safe']
        ];
    }

    public function getVideoRecord($doMapping = true)
    {
        if ($doMapping) {
            $this->mapFormDataInVideo();
        }

        return $this->video;
    }

    public function getDirector()
    {
        return Director::findOne($this->directorId);
    }

    public function getCountry()
    {
        return Country::findOne($this->countryId);
    }

    public function getActors()
    {
        if (empty($this->actorIds)) {
            return [];
        }

        return Actor::findAll($this->actorIds);

    }

    private function mapFormDataInVideo()
    {
        $this->video->title = $this->title;
        $this->video->origin_title = $this->originTitle;
        $this->video->annonce = $this->annonce;
        $this->video->description = $this->description;
        $this->video->setDuration($this->duration);
        $this->video->setPremiereDate(\DateTime::createFromFormat(self::PREMIERE_DATE_FORMAT, $this->premiereDate));
        $this->video->setFilmingPeriod($this->yearStartFilming, $this->yearEndFilming);
    }

    private function initFormData(Video $record)
    {
        $this->video = $record;
        $this->title = $record->title;
        $this->originTitle = $record->origin_title;
        $this->annonce = $record->annonce;
        $this->description = $record->description;
        $this->duration = $record->getDuration();

        $premiereDate = $record->getPremiereDate() ?: new \DateTime();
        $this->premiereDate = $premiereDate->format(self::PREMIERE_DATE_FORMAT);

        list($this->yearStartFilming, $this->yearEndFilming) = $record->getFilmingPeriod();

        $this->actorIds = ArrayHelper::getColumn($record->actors, 'id');
        $this->directorId = ArrayHelper::getValue($record->director, 'id', null);
        $this->countryId = ArrayHelper::getValue($record->country, 'id', null);
    }

    public function isCreationForm()
    {
        return $this->video->isNewRecord;
    }

    public function getDirectorList()
    {
        $directors = Director::find()->all();
        return ArrayHelper::map($directors, 'id', 'name');
    }

    public function getCountryList()
    {
        $countries = Country::find()->all();
        return ArrayHelper::map($countries, 'id', 'name');
    }

    public function getActorList()
    {
        $actors = Actor::find()->all();
        return ArrayHelper::map($actors, 'id', 'name');
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название фильма',
            'originTitle' => 'Оригинальное название',
            'yearStartFilming' => 'Год начала съемок',
            'yearEndFilming' => 'Год окончания съемок',
            'duration' => 'Продолжительность в секундах',
            'premiereDate' => 'Дата премьеры',
            'annonce' => 'Анонс',
            'description' => 'Описание',
            'actorIds' => 'Список актеров',
            'countryId' => 'Страна съемок',
            'directorId' => 'Режиссер'
        ];
    }
}
