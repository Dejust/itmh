<?php

namespace app\models\video\web;

use app\models\image\PicUploader;
use app\models\image\web\ImageUploadForm;
use app\models\video\ComplexVideoSaver;
use yii\base\Model;
use yii\web\UploadedFile;

class ComplexVideoForm extends Model
{
    /** @var  ImageUploadForm */
    public $pic;

    /** @var  VideoForm */
    public $video;

    public function validate($attributeNames = null, $clearErrors = true)
    {
        return $this->video->validate();
    }

    public function load($data, $formName = null)
    {
        $loaded = $this->pic->load($data, $formName);
        $loaded &= $this->video->load($data, $formName);
        return $loaded;
    }

    public function save()
    {
        $this->pic->imageFile = UploadedFile::getInstance($this->pic, 'imageFile');
        $uploadedFile = $this->pic->getUploadedImage();

        if ($this->validate() && $uploadedFile !== false) {
            $saver = new ComplexVideoSaver($this->video->getVideoRecord());

            $saver->director = $this->video->getDirector();
            $saver->country = $this->video->getCountry();
            $saver->actors = $this->video->getActors();

            if ($uploadedFile) {
                $saver->pic = (new PicUploader())->upload($uploadedFile);
            }

            $saver->save();

            return true;
        } else {
            return false;
        }
    }

    public function isCreationForm()
    {
        return $this->video->isCreationForm();
    }
}
