-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 06 2015 г., 16:40
-- Версия сервера: 5.6.15-log
-- Версия PHP: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `itmh3`
--

-- --------------------------------------------------------

--
-- Структура таблицы `actor`
--

CREATE TABLE IF NOT EXISTS `actor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `actor`
--

INSERT INTO `actor` (`id`, `name`) VALUES
(1, 'Актер А'),
(2, 'Актер Б'),
(3, 'Актер В'),
(4, 'Актер Г');

-- --------------------------------------------------------

--
-- Структура таблицы `actor_video`
--

CREATE TABLE IF NOT EXISTS `actor_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actor_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_actor_video__actor` (`actor_id`),
  KEY `FK_actor_video__video` (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `actor_video`
--

INSERT INTO `actor_video` (`id`, `actor_id`, `video_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Россия'),
(2, 'США'),
(3, 'Англия'),
(4, 'Япония');

-- --------------------------------------------------------

--
-- Структура таблицы `country_video`
--

CREATE TABLE IF NOT EXISTS `country_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_country_video__county` (`country_id`),
  KEY `FK_country_video__video` (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `country_video`
--

INSERT INTO `country_video` (`id`, `country_id`, `video_id`) VALUES
(2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `director`
--

CREATE TABLE IF NOT EXISTS `director` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `director`
--

INSERT INTO `director` (`id`, `name`) VALUES
(1, 'Директор А'),
(2, 'Директор Б'),
(3, 'Директор В'),
(4, 'Директор Г');

-- --------------------------------------------------------

--
-- Структура таблицы `director_video`
--

CREATE TABLE IF NOT EXISTS `director_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `director_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_director_video__director` (`director_id`),
  KEY `FK_director_video__video` (`video_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `director_video`
--

INSERT INTO `director_video` (`id`, `director_id`, `video_id`) VALUES
(2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1449416211),
('m151204_071717_create_user_table', 1449416212),
('m151204_101333_create_video_table', 1449416212),
('m151205_124013_create_director_table', 1449416215),
('m151205_125255_create_country_table', 1449416217),
('m151205_130146_create_actor_table', 1449416219),
('m151206_133239_add_pic_id_in_video_table', 1449416220),
('m151206_151903_add_test_data_in_dictionaries', 1449416222);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `password_hash`, `auth_key`) VALUES
(1, 'test@example.com', '$2y$13$xbVdXJKKMuxBbTXRe8I2XOlJcZMMEgAoMXS8zvpxitO1hC0klAbSK', 'jvWwj0bp-sIOZ9XNEJBpZaNvGxVWBkVX');

-- --------------------------------------------------------

--
-- Структура таблицы `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `origin_title` varchar(255) NOT NULL DEFAULT '',
  `year_start_filming` int(11) NOT NULL,
  `year_end_filming` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `premiere_date` date NOT NULL,
  `annonce` text NOT NULL,
  `description` text NOT NULL,
  `pic_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `video`
--

INSERT INTO `video` (`id`, `title`, `origin_title`, `year_start_filming`, `year_end_filming`, `duration`, `premiere_date`, `annonce`, `description`, `pic_id`) VALUES
(1, '123', '123', 1870, 1888, 213, '2015-12-06', '123', '123', '56645665c2e78');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `actor_video`
--
ALTER TABLE `actor_video`
  ADD CONSTRAINT `FK_actor_video__video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_actor_video__actor` FOREIGN KEY (`actor_id`) REFERENCES `actor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `country_video`
--
ALTER TABLE `country_video`
  ADD CONSTRAINT `FK_country_video__video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_country_video__county` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `director_video`
--
ALTER TABLE `director_video`
  ADD CONSTRAINT `FK_director_video__video` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_director_video__director` FOREIGN KEY (`director_id`) REFERENCES `director` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
