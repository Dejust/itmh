<?php

namespace app\exceptions;

use yii\base\ErrorException;
use yii\db\BaseActiveRecord;

class PersistenceException extends ErrorException
{
    /** @var  BaseActiveRecord */
    public $model;

    public function __construct(BaseActiveRecord $record, $message = '', $code = 0, $severity = 1, $filename = __FILE__, $lineno = __LINE__, \Exception $previous = null)
    {
        parent::__construct($message, $code, $severity, $filename, $lineno, $previous);
        $this->model = $record;
    }
}
