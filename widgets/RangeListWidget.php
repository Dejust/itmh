<?php

namespace app\widgets;

use yii\helpers\Html;
use yii\widgets\InputWidget;

class RangeListWidget extends InputWidget
{
    public $min = 10;
    public $max = 100;
    public $step = 1;
    public $options = [];
    public function run()
    {
        $list = range($this->min, $this->max, $this->step);

        return Html::activeDropDownList(
            $this->model,
            $this->attribute,
            array_combine($list, $list),
            $this->options
        );
    }
}

