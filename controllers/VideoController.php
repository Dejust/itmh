<?php
namespace app\controllers;

use app\models\image\web\ImageUploadForm;
use app\models\video\Video;
use app\models\video\web\ComplexVideoForm;
use app\models\video\web\VideoForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class VideoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $dataProvider = Yii::createObject(ActiveDataProvider::className(), [[
            'query' => Video::find()
        ]]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = Yii::createObject(ComplexVideoForm::className(), [[
            'video' => Yii::createObject(VideoForm::className(), [Yii::createObject(Video::className())]),
            'pic' => Yii::createObject(ImageUploadForm::className())
        ]]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Yii::createObject(ComplexVideoForm::className(), [[
            'video' => Yii::createObject(VideoForm::className(), [$this->findVideo($id)]),
            'pic' => Yii::createObject(ImageUploadForm::className())
        ]]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionDelete($id)
    {
        $this->findVideo($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Video
     * @throws NotFoundHttpException
     */
    private function findVideo($id)
    {
        if ($video = Video::findOne($id)) {
            return $video;
        }

        throw new NotFoundHttpException('Video not found');
    }
}
