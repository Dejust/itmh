<?php
/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 04.12.2015
 * Time: 12:42
 */

namespace app\controllers;


use app\models\user\web\SignInForm;
use app\models\user\web\SignUpForm;
use Yii;
use yii\web\Controller;

class AuthController extends Controller
{
    public function actionSignIn()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        /** @var SignInForm $model */
        $model  = Yii::createObject(SignInForm::className());

        if ($model->load(Yii::$app->request->post()) && $model->signIn()) {
            return $this->goBack();
        }

        return $this->render('sign-in', [
            'model' => $model
        ]);
    }

    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goBack();
        }

        /** @var SignUpForm $model */
        $model = Yii::createObject(SignUpForm::className());

        if ($model->load(Yii::$app->request->post()) && $model->signUp()) {
            return $this->goBack();
        }

        return $this->render('sign-up', [
            'model' => $model
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect($this->goHome());
    }
}
