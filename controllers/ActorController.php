<?php

namespace app\controllers;

use app\models\actor\Actor;
use Yii;

/**
 * ActorController implements the CRUD actions for Actor model.
 */
class ActorController extends CrudController
{
    public function init()
    {
        $this->modelClass = Actor::className();
        parent::init();
    }
}
