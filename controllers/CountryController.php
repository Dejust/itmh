<?php

namespace app\controllers;

use Yii;
use app\models\country\Country;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends CrudController
{
    public function init()
    {
        $this->modelClass = Country::className();
        parent::init();
    }
}
