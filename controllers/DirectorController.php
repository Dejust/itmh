<?php

namespace app\controllers;

use Yii;
use app\models\director\Director;

/**
 * DirectorController implements the CRUD actions for Director model.
 */
class DirectorController extends CrudController
{
    public function init()
    {
        $this->modelClass = Director::className();
        parent::init();
    }
}
