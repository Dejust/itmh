<?php
/** @var $dataProvider \yii\data\ActiveDataProvider */
use app\models\image\Pic;
use app\models\video\Video;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;

?>

<?= Html::a('Добавить новое видео', ['video/create'], ['class' => 'btn btn-primary'])?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => SerialColumn::className(),],
        [
            'label' => 'Название фильма',
            'attribute' => 'title'
        ],
        [
            'label' => 'Оригинальное название',
            'attribute' => 'origin_title',
        ],
        [
            'label' => 'Годы съемок',
            'attribute' => 'year_start_filming',
            'value' => function (Video $model) {
                return $model->year_start_filming . ' - ' . $model->year_end_filming;
            }
        ],
        [
            'label' => 'Продолжительность (секунд)',
            'attribute' => 'duration',
        ],
        [
            'label' => 'Дата премьеры',
            'attribute' => 'premiere_date',
            'value' => function (Video $model) {
                return $model->getPremiereDate()->format('d.m.Y');
            }
        ],
        [
            'label' => 'Облолжка',
            'value' => function (Video $model) {
                if ($pic = $model->getPic()) {
                    $img = Html::img($pic->getThumbnailPath(Pic::THUMBNAIL_SMALL));
                    return Html::a($img, $pic->getOriginPath());
                } else {
                    return null;
                }
            },
            'format' => 'raw'
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template' => '{update} {delete}'
        ]
    ]
])?>
