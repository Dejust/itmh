<?php
/** @var VideoForm $model */

use app\models\video\web\VideoForm;
use app\widgets\RangeListWidget;
use kartik\date\DatePicker;

?>

<?= $form->field($model, 'title')?>

<?= $form->field($model, 'originTitle')?>

<?php
$rangeListOptions = [
    'min' => VideoForm::MIN_FILMING_PERIOD_YEAR,
    'max' => VideoForm::MAX_FILMING_PERIOD_YEAR,
    'step' => 1
];
?>
<?=$form->field($model, 'yearStartFilming')->widget(RangeListWidget::className(), $rangeListOptions)?>
<?=$form->field($model, 'yearEndFilming')->widget(RangeListWidget::className(), $rangeListOptions)?>

<?=$form->field($model, 'duration')?>

<?=$form->field($model, 'premiereDate')->widget(DatePicker::className(), [
    'pluginOptions' => [
        'format' => 'dd.mm.yyyy'
    ]
])?>,

<?=$form->field($model, 'annonce')->textarea()?>

<?=$form->field($model, 'description')->textarea()?>

<?=$form->field($model, 'actorIds')->checkboxList($model->getActorList())?>

<?=$form->field($model, 'directorId')->dropDownList($model->getDirectorList())?>

<?=$form->field($model, 'countryId')->dropDownList($model->getCountryList())?>