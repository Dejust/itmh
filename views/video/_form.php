<?php
/** @var $model \app\models\video\web\ComplexVideoForm*/
use app\models\video\web\VideoForm;
use app\widgets\RangeListWidget;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);

?>

<?=$this->render('_form_video', [
    'model' => $model->video,
    'form' => $form
])?>

<?=$this->render('_form_pic', [
    'model' => $model->pic,
    'form' => $form
])?>

<?= Html::submitButton($model->isCreationForm() ? 'Создать' : 'Обновить')?>

<?php if ($pic = $model->video->getVideoRecord(false)->getPic()): ?>
    <h3>Pics</h3>
    <?=Html::a('Origin Pic', $pic->getOriginPath())?>
    <?php foreach ($pic->getThumbnailPaths() as $kind => $thumbPath):?>
        <?=Html::a($kind . ' Pic', $thumbPath)?>
    <?php endforeach;?>
<?php endif;?>

<?php $form->end(); ?>
