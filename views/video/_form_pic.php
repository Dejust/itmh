<?php
/** @var \app\models\image\web\ImageUploadForm model */
/** @var \yii\widgets\ActiveForm $form */
?>

<?=$form->field($model, 'imageFile')->fileInput(['accept' => 'image/*'])?>
