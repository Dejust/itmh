<?php
/** @var \app\models\user\web\SignUpForm $model */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

?>

<?=$form->field($model, 'email')?>
<?=$form->field($model, 'password')->passwordInput()?>
<?=$form->field($model, 'confirmedPassword')->passwordInput()?>
<?=$form->field($model, 'rememberMe')->checkbox()?>

<?= Html::submitButton('Sign Up')?>

<?php $form->end(); ?>
