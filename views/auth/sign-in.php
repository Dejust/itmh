<?php
/** @var \app\models\user\web\SignInForm $model */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

?>

<?=$form->field($model, 'email')?>
<?=$form->field($model, 'password')->passwordInput()?>
<?=$form->field($model, 'rememberMe')->checkbox()?>

<?=Html::submitButton('Sign In')?>

<?=Html::a('Or Sign Up', ['auth/sign-up'])?>

<?php $form->end(); ?>
